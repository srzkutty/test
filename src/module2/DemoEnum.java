//enum inside class
 package module2;
public class DemoEnum {
    public final String comName = "AMSTEEL";

    public String name = "azman";
    enum levels{
        low,medium,high
    }
    public static void  main(String[] args) {
        levels l  = levels.medium;
        System.out.println(l);
        DemoEnum de = new DemoEnum();

        de.name= "abu";

        switch (l){
            case low :
            System.out.println("low level");
            break;
            case medium:
                System.out.println("medium level");
                break;
            case high:
                System.out.println("high level");
                break;
            default :
                System.out.println("No value defined");

        }

        for (levels level : levels.values()){
            System.out.println("level = " + level);

        }
    }
}


