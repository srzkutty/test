package module2;

public class DemoEnum2 {
    public static void main (String[]args) {
        System.out.println("Team Name = " + EPL.ARSENAL);

        for(Tuna t: Tuna.values()){
            System.out.printf("%s  \t %d \n", t.getDesc(), t.getYear());
        }
    }
}
