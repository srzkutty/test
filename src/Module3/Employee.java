package Module3;

import java.util.ArrayList;

public class Employee {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void listEmployee() {
        ArrayList<Employee> emps = new ArrayList<>();

        Employee e1 = new Employee();
        e1.setAge(25);
        e1.setName("azman");
        emps.add(e1);

        Employee e2 = new Employee();
        e2.setAge(25);
        e2.setName("azman");
        emps.add(e1);

        for (Employee e: emps) {
            System.out.println("Name = " + e.getName()  + "Age = " + e.getAge());
        }
    }
}
